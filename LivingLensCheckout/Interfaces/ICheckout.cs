﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LivingLensCheckout.Interfaces
{

    public interface ICheckout
    {
        void Scan(IItem item);
        double Total();
    }

}
