﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LivingLensCheckout.Interfaces
{
    public interface IItem
    {
        string name();
        double price();
        
    }
}
